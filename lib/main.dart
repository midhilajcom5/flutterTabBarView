import 'package:flutter/material.dart';
import 'package:flutter_app/Page.dart';

import 'page1.dart';

void main() => runApp(MyAppPages());

class MyApp extends StatefulWidget {
  String message;
  MyApp(this.message) {}
  @override
  _MyAppState createState() => _MyAppState(message);
}

class _MyAppState extends State<MyApp> {
  String message;
  _MyAppState(this.message) {}

  List<Page> data1 = [Page(title: "a1"), Page(title: "b1"), Page(title: "c1")];
  List<Page> data2 = [Page(title: "a2"), Page(title: "b2"), Page(title: "c2")];
  List<Page> data3 = [Page(title: "a3"), Page(title: "b3"), Page(title: "c3")];
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(

        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(child: Text(message)),
              Tab(child: Text(message)),
              Tab(child: Text(message))
            ],
          ),
        ),
        body: TabBarView(children: [
          Page1(
            data: data1,
          ),
          Page1(
            data: data2,
          ),
          Page1(
            data: data3,
          )
        ]),
      ),
    );
  }
}

class MyAppPages extends StatefulWidget {
  @override
  _MyAppPages createState() => _MyAppPages();
}

class _MyAppPages extends State<MyAppPages> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: (){

           this.setState(() {
              ct+=1;
            print("ct $ct");
            });
          }),
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Tab(
                    child: GestureDetector(
                      child: Text("page1"),
                      onTap: () {
                      this.  setState(() {
                          ct += 1;
                        });
                        print("ct $ct");
                  },
                )),
                Tab(child: Text("page2")),
                Tab(child: Text("page3"))
              ],
            ),
          ),
          body:
              TabBarView(children: [MyApp("sub_1_ct_$ct"),MyApp("sub_2_ct_$ct"), MyApp("sub_3_ct_$ct")]),
        ),
      ),
    );
  }

  int ct = 0;

}
