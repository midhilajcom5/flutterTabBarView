import 'package:flutter/material.dart';

import 'Page.dart';

class Page1 extends StatefulWidget {
  List<Page> data;
  Page1({this.data,Key key}) : super(key: key);

  @override
  _Page1State createState() => _Page1State(data:data);
}

class _Page1State extends State<Page1> {
  List<Page> data;
  _Page1State({this.data}){

  }
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context,index){
      return Container(
        margin: EdgeInsets.all(10),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListTile(
              title: Text(data[index].title),
            ),
          ),
        ),
      );
    });
  }
}